const path = require('path');
const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false
        }
    },
    chainWebpack: config => {
        // Remove prefetch plugin and that's it!
        config.plugins.delete('prefetch');
    },
    configureWebpack: {
        resolve: {
            alias: {
                '@themeConfig': path.resolve(__dirname, 'theme.config.js'),                
            }
        }
    },
    devServer: {
        host: '0.0.0.0',
        // https:true, 
        port: 5001,
        client: { webSocketURL: 'ws://0.0.0.0:5001/ws', },
        headers: { 'Access-Control-Allow-Origin': '*', }
      }, transpileDependencies: true
});